# Advent of Code 2020

## Day 1 - Dart: [Problem](https://adventofcode.com/2020/day/1), [Solution](day-1.dart)

## Day 2 - Haskell: [Problem](https://adventofcode.com/2020/day/2), [Solution](day-2.hs)

## Day 3 - Pure Script: [Problem](https://adventofcode.com/2020/day/3), [Solution](day-3/src/Main.purs)

## Day 4 - Java: [Problem](https://adventofcode.com/2020/day/4), [Solution](day-4.java)

## Day 5 - R: [Problem](https://adventofcode.com/2020/day/5), [Solution](day-5.r)

## Day 6 - F#: [Problem](https://adventofcode.com/2020/day/6), [Solution](day-6.fsx)

## Day 7 - C#: [Problem](https://adventofcode.com/2020/day/7), [Solution](day-7/Day7/Day7/Program.cs)

## Day 8 - C: [Problem](https://adventofcode.com/2020/day/8), [Solution](day-8.c)

## Day 9 - Kotlin: [Problem](https://adventofcode.com/2020/day/9), [Solution](day-9.kt)

## Day 10 - Javascript: [Problem](https://adventofcode.com/2020/day/10), [Solution](day-10.js)

## Day 12 - Ruby: [Problem](https://adventofcode.com/2020/day/12), [Solution](day-12.rb)

## Day 13 - Nim: [Problem](https://adventofcode.com/2020/day/13), [Solution](day13.nim)

## Day 14 - Go: [Problem](https://adventofcode.com/2020/day/14), [Solution](day-14.go)

## Day 15 - Elm: [Problem](https://adventofcode.com/2020/day/15), [Solution](day-15/src/Main.elm)

## Day 16 - TypeScript: [Problem](https://adventofcode.com/2020/day/16), [Solution](day-16.ts)

## Day 17 - Common Lisp: [Problem](https://adventofcode.com/2020/day/17), [Part one solution](day-17-part-one.lisp), [Part two solution](day-17-part-two.lisp)

## Day 18 - Scala: [Problem](https://adventofcode.com/2020/day/18), [Solution](day-18/src/main/scala/Main.scala)
