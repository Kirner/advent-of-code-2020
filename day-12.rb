def get_lines
    lines = []

    File.open('day-12-input.txt', 'r') do |f1|
        while line = f1.gets
            lines.append(line)
        end
    end
    return lines
end

def move(direction, val, north, east)
    case direction
    when "N"
        north += val
    when "E"
        east += val
    when "S"
        north -= val
    when "W"
        east -= val
    end
    return north, east
end

def perform_instruction_part_one(instruction, argument, north, east, heading)
    if ["N", "E", "S", "W"].include? instruction
        north, east = move(instruction, argument, north, east)
    end

    case instruction
    when "L"
        heading = (heading - argument) % 360
    when "R"
        heading = (heading + argument) % 360
    when "F"
        case heading
        when 0
            north += argument
        when 90
            east += argument
        when 180
            north -= argument
        when 270
            east -= argument
        end
    end
    return north, east, heading
end

def perform_instruction_part_two(instruction, argument, north, east, waypoint_north, waypoint_east)
    if ["N", "E", "S", "W"].include? instruction
        waypoint_north, waypoint_east = move(instruction, argument, waypoint_north, waypoint_east)
    end

    case instruction
    when "L"
        rotations = argument / 90
        for i in 1..rotations
            waypoint_east, waypoint_north = -waypoint_north, waypoint_east
        end
    when "R"
        rotations = argument / 90
        for i in 1..rotations
            waypoint_east, waypoint_north = waypoint_north, -waypoint_east
        end
    when "F"            
        north += waypoint_north * argument
        east += waypoint_east * argument
    end
    return north, east, waypoint_north, waypoint_east
end

def part_one(lines)
    east = 0
    north = 0
    heading = 90
    lines.each { |line|
        instruction = line[0]
        argument = line[1..]
        north, east, heading = \
            perform_instruction_part_one(instruction, argument.to_i, north, east, heading)
    }
    return east.abs + north.abs
end

def part_two(lines)
    heading = 90
    east, north = 0, 0
    waypoint_east, waypoint_north = 10, 1

    lines.each { |line|
        instruction = line[0]
        argument = line[1..]
        north, east, waypoint_north, waypoint_east = \
            perform_instruction_part_two(instruction, argument.to_i, north, east, waypoint_north, waypoint_east)
    }
    return east.abs + north.abs
end

lines = get_lines
puts ("Part One: " + String(part_one(lines)))
puts ("Part Two: " + String(part_two(lines)))