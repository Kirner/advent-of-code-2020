import java.io.File

fun main() {
    val lines = File("day-9-input.txt").readLines()
    val partOneNum = partOne(lines)
    println("Part One: $partOneNum")
    println("Part Two: " + partTwo(lines, partOneNum))
}

fun partTwo(lines: List<String>, partOneAnswer: Long): Long {
    for (lowerNumber in lines) {
        var totalSum = lowerNumber.toLong()
        for (higherNumber in lines.subList(lines.indexOf(lowerNumber)+1, lines.count())) {
            totalSum += higherNumber.toLong()
            if (totalSum != partOneAnswer) continue
            val range = lines.subList(lines.indexOf(lowerNumber), lines.indexOf(higherNumber)).map { o -> o.toLong() }
            return (range.minOrNull() ?: 0) + (range.maxOrNull() ?: 0)
        }
    }
    return -1
}

fun partOne(lines: List<String>): Long {
    for (i in 25 until lines.count()) {
        val last25 = lines.subList(i-25, i)

        var foundSum = false;
        for (x in last25) {
            for (y in last25) {
                if (x == y) continue
                if ((x.toLong() + y.toLong()) == lines[i].toLong())
                    foundSum = true
            }
            if (foundSum) break
        }

        if (!foundSum) {
            return lines[i].toLong()
        }
    }

    return -1;
}
