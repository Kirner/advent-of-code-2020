getFileLines <- function(filename) {
  conn <- file(filename,open="r")
  lines <-readLines(conn)
  close(conn)
  lines
}

lines <- getFileLines("day-5-input.txt")

getRow <- function (boardingPassDescriptor){
  rowDescriptor <- substring(boardingPassDescriptor, 1, 7)
  rowDescriptor <- gsub("B", "1", rowDescriptor)
  rowDescriptor <- gsub("F", "0", rowDescriptor)
  strtoi(rowDescriptor, base = 2)
}

getColumn <- function (boardingPassDescriptor){
  len = nchar(boardingPassDescriptor)
  columnDescriptor <- substring(boardingPassDescriptor, len - 2, len)
  columnDescriptor <- gsub("R", "1", columnDescriptor)
  columnDescriptor <- gsub("L", "0", columnDescriptor)
  strtoi(columnDescriptor, base = 2)
}

getSeatId <- function (boardingPassDescriptor){
  getRow(boardingPassDescriptor) * 8 + getColumn(boardingPassDescriptor)
}

ids <- sapply(lines, getSeatId)
highestId <- sort(ids)[length(ids)]


for (n in 1:highestId) {
  if (n %in% ids) next
  if ((n+1) %in% ids && ((n-1) %in% ids)) {
    part2 <- n
  }
}

sprintf("Part 1: %d", highestId)
sprintf("Part 2: %d", part2)
