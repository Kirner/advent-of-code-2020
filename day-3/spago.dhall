{-
Welcome to a Spago project!
You can edit this file as you like.
-}
{ name = "my-project"
, dependencies =
  [ "console"
  , "effect"
  , "encoding"
  , "lists"
  , "longs"
  , "node-buffer"
  , "node-fs"
  , "numbers"
  , "psci-support"
  , "stringutils"
  , "uint"
  ]
, packages = ./packages.dhall
, sources = [ "src/**/*.purs", "test/**/*.purs" ]
}
