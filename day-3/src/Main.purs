module Main where
import Prelude (Unit, bind, (==), discard)
import Effect (Effect)
import Effect.Console (log)
import Node.Encoding (Encoding(..))
import Node.FS.Sync (readTextFile)
import Data.String.Utils (lines)
import Data.String.CodeUnits (charAt)
import Data.Array (filter, length, zip, (!!), (..))
import Data.Maybe (fromMaybe)
import Data.Tuple (Tuple(..), fst, snd)
import Data.Int (toNumber)
import Data.EuclideanRing (mod)
import Data.Ring ((-))
import Data.Semiring ((*))
import Data.Functor (map)
import Data.Ord((>))
import Data.Show

numberedLineAtIndex :: Array (Tuple Int String) -> Int -> (Tuple Int String)
numberedLineAtIndex a n = fromMaybe (Tuple 0 "str") (a !! n)

main :: Effect Unit
main = do
  inputText <- readTextFile UTF8 "day-3-input.txt"
  log "Part one:"
  log (show (findTreesInPath inputText 1 3))
  log "Part Two:"
  log (show (partTwo inputText))

partTwo :: String -> Number
partTwo inputText = anwser where
  a1 = toNumber (findTreesInPath inputText 1 1)
  a2 = toNumber (findTreesInPath inputText 1 3)
  a3 = toNumber (findTreesInPath inputText 1 5)
  a4 = toNumber (findTreesInPath inputText 1 7)
  a5 = toNumber (findTreesInPath inputText 2 1)
  anwser = a1 * a2 * a3 * a4 * a5

findTreesInPath :: String -> Int -> Int -> Int
findTreesInPath inputText stepsDown stepsRight = numberOfTrees where
  splitLines = (lines inputText)
  unfilteredNumberedLines = (zip (1 .. (length splitLines)) splitLines)
  relevantLines = map snd (filter (keepLine stepsDown) (unfilteredNumberedLines))
  relevantNumberedLines = (zip (1 .. (length relevantLines)) relevantLines)

  withTrees = filter (checkLineForTree stepsRight) relevantNumberedLines
  numberOfTrees = length withTrees 

keepLine :: Int -> Tuple Int String -> Boolean
keepLine stepsDown t = ((fst t-1) `mod` stepsDown) == 0

checkLineForTree :: Int -> Tuple Int String -> Boolean
checkLineForTree stepsRight t = hasTreeAtIndex str index where
  lineNum = fst t
  str = snd t 
  index = getIndexFromLineNumber lineNum stepsRight

getIndexFromLineNumber :: Int -> Int -> Int
getIndexFromLineNumber lineNum stepsRight = if ((lineNum*stepsRight)-stepsRight) > 30 
  then (((lineNum*stepsRight)-stepsRight) `mod` 31)
  else ((lineNum*stepsRight)-stepsRight)

hasTreeAtIndex :: String -> Int -> Boolean
hasTreeAtIndex str n = (fromMaybe ' ' (charAt n str)) == '#'