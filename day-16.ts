import * as fs from 'fs';
import {EOL} from 'os';

function parseCriteria(input: string): { [label: string]: Function } {
    const criteria = input.split("your ticket:")[0].trim().split(EOL);
    const criteriaByLabel = {};

    criteria.forEach(c => {
        const [label, bounds] = c.split(": ");
        const [lowerRangeStr, upperRangeStr] = bounds.split(" or ");

        const lowerRange = lowerRangeStr.split("-").map(n => Number(n));
        const upperRange = upperRangeStr.split("-").map(n => Number(n));
        const checkLowerRange = (n: number) => (n >= lowerRange[0] && n <= lowerRange[1]);
        const checkUpperRange = (n: number) => (n >= upperRange[0] && n <= upperRange[1]);

        const fitsCriteria = (n: number) => (checkLowerRange(n) || checkUpperRange(n));
        criteriaByLabel[label] = fitsCriteria;
    })

    return criteriaByLabel;
}

function getTicket(input: string): Array<number> {
    const ticketStr = input.split("your ticket:")[1].split("nearby tickets:")[0];
    return ticketStr.split(",").map(Number);
}

function parseTickets(input: string): Array<Array<number>> {
    const ticketLines = inputText.split("nearby tickets:")[1].trim().split(EOL);
    const tickets = ticketLines.map(o => o.split(","));
    return tickets.map(ticket => ticket.map(n => Number(n)));
}

function meetsCriteria(num: Number, criteria: { [label: string]: Function }) {
    for (const label in criteria) {
        if (criteria[label](num)) return true;
    }
    return false;
}

function ticketMeetsCriteria(ticket: Array<Number>, criteria: {[label: string]: Function }): boolean {
    let valid = true;
    ticket.forEach(num => {
        if (meetsCriteria(num, criteria)) return;
        valid = false;
    })
    return valid;
}

function criteriaFilteredByIndex(tickets: Array<Array<number>>, criteria: { [label: string]: Function }): { [index: number]: Array<string> } {
    let possibleLabelsByIndex = {}
    const labels = Object.keys(criteria)
    for (let i = 0; i < labels.length; i++) {
        possibleLabelsByIndex[i] = [...labels];
    }

    tickets.forEach(ticket => {
        for (let i = 0; i < ticket.length; i++){
            for (let label in criteria) {
                if (criteria[label](ticket[i])) continue;
                const index = possibleLabelsByIndex[i].indexOf(label);
                if (index > -1) {
                    possibleLabelsByIndex[i].splice(index, 1);
                }
            }
        }
    });

    return possibleLabelsByIndex;
}

function removeLabel(labelsByIndex: { [index: number]: Array<string> }, label: string): { [index: number]: Array<string> } {
    for (let index in labelsByIndex) {
        const i = labelsByIndex[index].indexOf(label);
        if (i < 0) continue;
        labelsByIndex[index].splice(i, 1);
    }

    return labelsByIndex;
}

function filterLabelByIndex(indexesToPosibleLabels: { [index: number]: Array<string> }): { [label: string]: number } {
    const indexesByLabel = {};
    for (let _ in indexesToPosibleLabels ) {
        for (let index in indexesToPosibleLabels ) {
            if (indexesToPosibleLabels[index].length == 1) {
                const str = indexesToPosibleLabels[index][0]
                indexesToPosibleLabels = removeLabel(indexesToPosibleLabels, str);
                indexesByLabel[str] = Number(index);
            }
        }
    }
    return indexesByLabel;
}

function partOne(input: string): Number {
    const tickets = parseTickets(inputText);
    const criteria = parseCriteria(inputText);

    let total = 0;
    tickets.forEach(ticket => {
        ticket.forEach(num => {
            if (meetsCriteria(num, criteria)) return;
            total+=num;
        });
    });
    return total;
}

function partTwo(input: string): Number {
    const tickets = parseTickets(input);
    const criteria = parseCriteria(input);
    const filterdTickets = tickets.filter(t => ticketMeetsCriteria(t, criteria));
    const indexesToPosibleLabels = criteriaFilteredByIndex(filterdTickets, criteria);
    const indexesByLabel = filterLabelByIndex(indexesToPosibleLabels);
    const ticketValues = getTicket(input);

    let anwser = 1;
    for (let label in indexesByLabel) {
        if (!label.includes("departure")) continue;
        anwser = anwser * ticketValues[indexesByLabel[label]];
    }
    return anwser;
}

const inputText = fs.readFileSync("day-16-input.txt").toString();
console.log("Part One: " + partOne(inputText));
console.log("Part Two: " + partTwo(inputText));