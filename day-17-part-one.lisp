(defstruct 3d-point x y z)

(defun make-line (len)
  (make-string len :initial-element '#\.))

(defun make-2d-grid (size)
  (make-list size :initial-element (make-line size)))

(defun make-3d-grid (2d-grid)
  (let ((size (length 2d-grid)))
    (list (make-2d-grid size) 2d-grid (make-2d-grid size))))

(defun expand-x (grid)
  (loop for 2d-grid in grid
        collect (loop for row in 2d-grid
                      collect (concatenate 'string "." row "."))))

(defun expand-y (grid)
  (let ((line (list (make-line (length (car (car grid)))))))
    (loop for 2d-grid in grid
          collect (append line 2d-grid line))))

(defun expand-z (grid)
  (let ((new-face (list (make-2d-grid (length (car grid))))))
    (append new-face grid new-face)))

(defun grow (grid)
  (expand-z (expand-y (expand-x grid))))

(defun active-in-grid (grid)
  (apply '+ (loop for lines in grid collect
                  (apply '+ (loop for line in lines collect
                                  (count #\# line))))))

(defun 1d-neighbour-coordinates (point)
  (list (- point 1) point (+ point 1)))

(defun neighbour-coordinates (point)
  (loop for z in (1d-neighbour-coordinates (3d-point-z point))
        append (loop for y in (1d-neighbour-coordinates (3d-point-y point))
                     append (loop for x in (1d-neighbour-coordinates (3d-point-x point))
                                  for new-point = (make-3d-point :x x :y y :z z)
                                  if (not (equalp new-point point))
                                    collect new-point))))

(defun nth-or-nil (n li)
  (cond ((< n 0) nil)
        ((>= n (length li)) nil)
        ((equal li nil) nil)
        ((typep li 'list) (nth n li))
        ((typep li 'string) (char li n))))

(defun active-cell (point grid)
  (let* ((face (nth-or-nil (3d-point-z point) grid))
         (row (nth-or-nil (3d-point-y point) face))
         (cell (nth-or-nil (3d-point-x point) row)))
      (equal #\# cell)))

(defun active-neighbours (point grid)
  (length (let ((neighbour-coords (neighbour-coordinates point)))
    (remove-if-not #'(lambda (x) (active-cell x grid)) neighbour-coords))))

(defun next-cell-state (point grid)
  (let* ((num-neighbours (active-neighbours point grid))
         (active (active-cell point grid)))
      (cond ((and active
                  (or (= num-neighbours 2)
                      (= num-neighbours 3))) #\#)
            ((and (not active)
                  (= num-neighbours 3)) #\#)
            (t #\.))))

(defun next-state (grid)
  (let* ((z-length (- (length grid) 1))
         (y-length (- (length (car grid)) 1))
         (x-length (- (length (car (car grid))) 1)))
    (loop for z from 0 to z-length
          collect (loop for y from 0 to y-length
                        collect (coerce (loop for x from 0 to x-length
                                              collect (let* ((p (make-3d-point :x x :y y :z z)))
                                                        (next-cell-state p grid))) 'string)))))

(defun read-lines (filepath)
  (with-open-file (in filepath)
    (loop for line = (read-line in nil nil)
      while line
      collect line)))

(defun anwser (grid n)
  (if (= n 6)
      (active-in-grid grid)
      (anwser (next-state (grow grid)) (+ n 1))))

(defparameter 3d-grid (make-3d-grid (read-lines "day-17-input.txt")))
(write (anwser 3d-grid 0))
