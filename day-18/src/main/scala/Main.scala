import scala.io.Source
import scala.annotation.tailrec
import scala.util.matching.Regex

@main def day18: Unit =
  val inputLines = Source.fromFile("./day-18-input.txt").getLines().toList
  val partOneAnwser =  inputLines.map(partOneSolution).sum
  val partTwoAnwser =  inputLines.map(partTwoSolution).sum
  println("-------------------------")
  println("Part one anwser:")
  println(partOneAnwser)
  println("\nPart two anwser:")
  println(partTwoAnwser)
  println("-------------------------")

// ############ Part One ############

@tailrec
def partOneSolution(problem: String): Long = {
  val statements = statementsInBrackets(problem)
  var p = problem
  for (statement <- statements)
    p = p.replace(statement, solveInOrder(statement.tail.init))
  if (p.contains("("))
    partOneSolution(p)
  else {solveInOrder(p).toLong}
}

// ############ Part Two ############

@tailrec
def partTwoSolution(problem: String): Long = {
  val statements = statementsInBrackets(problem)
  var p = problem
  for (statement <- statements)
    p = p.replace(statement, solveWithPrecedence(statement.tail.init))
  if (p.contains("("))
    partTwoSolution(p)
  else {solveWithPrecedence(p).toLong}
}

def solveWithPrecedence(problem: String): String = {
  val resolvedAdditions = if (problem.contains("+"))
    resolveAdditions(problem.split(" ").toList)
  else problem

  if (resolvedAdditions.contains("*"))
    solveInOrder(resolvedAdditions)
  else resolvedAdditions
}

@tailrec
def resolveAdditions(tokens: List[String]): String = {
  val untilAddition = tokens.takeWhile(o => o != "+")
  val afterAddtion = tokens.dropWhile(o => o != "+").tail
  val op = Operation(untilAddition.last.toLong, afterAddtion.head.toLong, '+')
  val anwser = op.resolve().toString
  val newTokens = (untilAddition.init :+ anwser) ++ afterAddtion.tail

  if (newTokens.contains("+"))
    resolveAdditions(newTokens)
  else newTokens.mkString(" ")
}

// ############ Common ############

val statementsInBrackets = (problem: String) => {
  val bracketPattern: Regex = "\\([^()]*\\)".r

  bracketPattern.findAllMatchIn(problem)
    .toList
    .map(o => o.toString)
}

class Operation(val n1: Long, val n2: Long, val op: Char) {
  def resolve(): Long = {
    if (op == '+') n1 + n2
    else n1 * n2
  }

  override def toString: String = s"$n1 $op $n2"
}

def solveInOrder(problem: String): String = {
  @tailrec
  def solve(tokens: List[String]): List[String] = tokens match {
    case n1 :: op :: n2 :: xs => {
      val operation = Operation(n1.toLong, n2.toLong, op.head)
      val anwser = operation.resolve().toString
      solve(anwser :: xs)
    }
    case _ => tokens
  }
  val tokens = problem.split(" ").toList
  solve(tokens).mkString(" ")
}
