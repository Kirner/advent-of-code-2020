open System.IO

let getGroups (filepath:string) = Seq.toList((System.IO.File.ReadAllText filepath).Split "\n\n")

let countChar xs x = Seq.filter ((=) x) xs |> Seq.length

let countOccuringLetters (group:string) = 
    let letters = ['a' .. 'z']
    let counts = List.map (countChar group) letters
    List.filter ((<>) 0) counts |> List.length

let letterOccursOnEveryLine lines letter = 
    let flipedArgumentCount x y = countChar y x
    let counts = List.map (flipedArgumentCount letter) lines
    if List.contains 0 counts then 0 else 1

let countOccuringLettersInEveryLine (group:string) = 
    let lines = group.Split('\n') |> Seq.toList
    let letters = ['a'..'z']
    let counts = List.map (letterOccursOnEveryLine lines) letters
    List.filter ((<>) 0) counts |> List.length

let fileLines = getGroups "day-6-input.txt"
let partOne = List.sum (List.map countOccuringLetters fileLines)
let partTwo = List.sum (List.map countOccuringLettersInEveryLine fileLines)
printfn "Part One: %A" partOne
printfn "Part Two: %A" partTwo