#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

int readLines(char* linesBuffer[], char* filename, int len){
    FILE *fptr = NULL;
    fptr = fopen(filename, "r");

    char buffer[len];

    int i = 0;
    while(fgets(buffer, len, fptr)) {
        linesBuffer[i] = (char*)malloc(10);
        strcpy(linesBuffer[i], buffer);
        i++;
    }

    fclose(fptr);
    return i;
}

bool visited(int linesVisited[], int line, int visitedLen){
    for (int i=0; i < visitedLen; i++ ) {
        if (linesVisited[i] == line) return true;
    }
    return false;
}

int counterOrZero(char* linesBuffer[], int length) {
    int counter = 0;
    int currentIndex = 0;
    int linesVisited[length];
    int visitedLength = 0;

    while (!visited(linesVisited, currentIndex, visitedLength)) {
        if (currentIndex >= length)
            return counter;

        linesVisited[visitedLength] = currentIndex;
        visitedLength++;
        char str[10];

        strcpy(str, linesBuffer[currentIndex]);
        char *instruction = strtok(str, " ");
        char *valueStr = strtok(NULL, " ");
        int value = atoi(valueStr);

        if (strcmp(instruction, "acc") == 0){
            counter += value;
            currentIndex++;
        }
        else if (strcmp(instruction, "jmp") == 0) {
            currentIndex += value;
        }
        else if (strcmp(instruction, "nop") == 0) {
            currentIndex++;
        }
    }

    return 0;
}

int partTwo(char* linesBuffer[], int length){
    for (int i; i < length; i++) {
        char oldLine [10];
        strcpy(oldLine, linesBuffer[i]);

        char str [10];
        strcpy(str, linesBuffer[i]);

        char *instruction = strtok(str, " ");
        char *valueStr = strtok(NULL, " ");
        if (strcmp(instruction, "jmp") == 0) {
            instruction = "nop ";
        }
        else if (strcmp(instruction, "nop") == 0) {
            instruction = "jmp ";
        }
        else if (strcmp(instruction, "acc") == 0) {
            instruction = "acc ";
        }

        char newLine[10];
        strcpy(newLine, instruction);
        strcat(newLine, valueStr);
        strcpy(linesBuffer[i], newLine);

        int count = counterOrZero(linesBuffer, length);
        if (count > 0){
            return count;
        }

        strcpy(linesBuffer[i], oldLine);
    }
    return -1;
}

int partOne(char* linesBuffer[], int length){
    int counter = 0;
    int currentIndex = 0;
    int linesVisited[length];
    int visitedLength = 0;

    while (!visited(linesVisited, currentIndex, visitedLength)) {
        linesVisited[visitedLength] = currentIndex;
        visitedLength++;
        char str[10];

        strcpy(str, linesBuffer[currentIndex]);
        char *instruction = strtok(str, " ");
        char *valueStr = strtok(NULL, " ");
        int value = atoi(valueStr);

        if (strcmp(instruction, "acc") == 0){
            counter += value;
            currentIndex++;
        }
        else if (strcmp(instruction, "jmp") == 0) {
            currentIndex += value;
        }
        else if (strcmp(instruction, "nop") == 0) {
            currentIndex++;
        }
    }

    return counter;
}

int main(void)
{
    char *lines[1000];
    int length = readLines(lines, "day-8-input.txt", 10);

    int partOneAnswer = partOne(lines, length);
    int partTwoAnswer = partTwo(lines, length);

    printf("\nPart One: %d", partOneAnswer);
    printf("\nPart Two: %d", partTwoAnswer);

    return 0;
}
