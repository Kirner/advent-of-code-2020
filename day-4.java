import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Scanner;
import java.util.Arrays;

class PassportFields {
    public static final String BirthYear = "byr";
    public static final String IssueYear = "iyr";
    public static final String ExpirationYear = "eyr";
    public static final String Height = "hgt";
    public static final String HairColor = "hcl";
    public static final String EyeColor = "ecl";
    public static final String PassportID = "pid";
    public static final String CountryID = "cid";
    public static final String[] mandatoryFields = {
        BirthYear, IssueYear, ExpirationYear, 
        Height, HairColor, EyeColor, PassportID
    };
}

class DayFour {
    public static void main(String[] args) {
        String filename = "day-4-input.txt";
        List<List<String>> groups = getGroups(filename);
        System.out.println("Part 1: "  + PartOne.Answer(groups));
        System.out.println("Part 2: "  + PartTwo.Answer(groups));
    }

    private static List<List<String>> getGroups(String filename){
        List<List<String>> groups = new ArrayList<List<String>>();
        try {
            File myObj = new File(filename);
            Scanner myReader = new Scanner(myObj);

            groups.add(new ArrayList<String>());
            while (myReader.hasNextLine()) {
                String line = myReader.nextLine();
                if (line.isEmpty()) {
                    groups.add(new ArrayList<String>());
                }
                else {                        
                    groups.get(groups.size()-1).add(line);
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("Could not find file: " + filename);
            e.printStackTrace();
        }
        return groups;
    }
}

class PartOne {
    public static int Answer(List<List<String>> groups){
        int answer = 0;
        for (List<String> group:groups){
            if (GroupIsValid(group)){
                answer++;
            }
        }
        return answer;
    }

    public static boolean GroupIsValid(List<String> group){
        String groupText = String.join("", group);
        for (String field:PassportFields.mandatoryFields) {
            if (!groupText.contains(field+":")) {
                return false;
            }
        }
        return true;
    }
}

class PartTwo {
    public static int Answer(List<List<String>> groups){
        int answer = 0;
        for (List<String> group:groups){
            Dictionary<String, String> pairs = getKeyValuePairs(String.join(" ", group));
            if (!PartOne.GroupIsValid(group)) continue;
            if (!pairsAreValid(pairs)) continue;
            answer++;
        }
        return answer;
    }

    private static boolean pairsAreValid(Dictionary<String, String> pairs){
        Enumeration<String> keys = pairs.keys();
        while (keys.hasMoreElements()){
            String key = keys.nextElement();
            switch (key) {
                case PassportFields.BirthYear:
                    int birthYear = valueAsInt(pairs.get(key));
                    if (!(birthYear <= 2002 && birthYear >= 1920)) return false;
                    break;
                case PassportFields.IssueYear:
                    int issueYear = valueAsInt(pairs.get(key));
                    if (!(issueYear <= 2020 && issueYear >= 2010)) return false;
                    break;
                case PassportFields.ExpirationYear:
                    int expirationYear = valueAsInt(pairs.get(key));
                    if (!(expirationYear <= 2030 && expirationYear >= 2020)) return false;
                    break;
                case PassportFields.Height:
                    if (!(pairs.get(key).contains("cm") | pairs.get(key).contains("in"))) return false;
                    if (pairs.get(key).contains("in")){
                        int height = valueAsInt(pairs.get(key).replace("in", ""));
                        if (!(height <= 76 && height >= 59)) return false;
                    }
                    if (pairs.get(key).contains("cm")){
                        int height = valueAsInt(pairs.get(key).replace("cm", ""));
                        if (!(height <= 193 && height >= 150)) return false;
                    }
                    break;
                case PassportFields.HairColor:
                    String hairColor = pairs.get(key);
                    if (hairColor.length() != 7) return false;
                    if (!hairColor.matches("#[a-f0-9]+")) return false;
                    break;
                case PassportFields.EyeColor:
                    List<String> eyeColors = new ArrayList<String>(Arrays.asList("amb", "blu", "brn", "gry", "grn", "hzl", "oth") );
                    if (!eyeColors.contains(pairs.get(key))) return false;
                    break;
                case PassportFields.PassportID:
                    String passportID = pairs.get(key);
                    if (!(passportID.length() == 9)) return false;
                    if (valueAsInt(passportID) < 0) return false;
                    break;
            }                       
        }
        return true;
    }

    private static int valueAsInt(String str){
        try { 
            return Integer.parseInt(str); 
        }
        catch (NumberFormatException e) {
            return -1;
        }
    }

    private static Dictionary<String, String> getKeyValuePairs(String groupText){
        Dictionary<String, String> pairs = new Hashtable<String, String>();
        for (String keyValueText : groupText.split("\\s+")) {
            String[] keyAndValue = keyValueText.split(":");
            assert (keyAndValue.length == 2);
            pairs.put(keyAndValue[0], keyAndValue[1]);
        }
        return pairs;
    }
}

