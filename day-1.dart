import 'dart:io';

void main() {
  new File(".\\day-1-input.txt").readAsString().then((String contents) {
    List<int> numbers =
        contents.split("\r\n").map((i) => int.parse(i)).toList();
    partOne(numbers);
    partTwo(numbers);
  });
}

void partOne(List<int> listOfNumbers) {
  for (var firstNumber in listOfNumbers)
    for (var secondNumber in listOfNumbers)
      if (firstNumber + secondNumber == 2020) {
        print("The number is: ${firstNumber * secondNumber}");
        return;
      }
}

void partTwo(List<int> listOfNumbers) {
  for (var firstNumber in listOfNumbers)
    for (var secondNumber in listOfNumbers)
      for (var thirdNumber in listOfNumbers)
        if (firstNumber + secondNumber + thirdNumber == 2020) {
          print("The number is: ${firstNumber * secondNumber * thirdNumber}");
          return;
        }
}
