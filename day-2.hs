import Debug.Trace
import Data.Bits

main = do
        contents <- readFile "day-2-input.txt"
        let linesOfFile = lines contents
        let matchingPartOne = filter matchesCriteriaPartOne linesOfFile
        let matchingPartTwo = filter matchesCriteriaPartTwo linesOfFile
        print "Part One:"
        print (length matchingPartOne)
        print "Part Two:"
        print (length matchingPartTwo)
        print (getPassword (linesOfFile !! 0))

matchesCriteriaPartOne :: String -> Bool
matchesCriteriaPartOne str = matches where
    (minAmount, maxAmount, letter) = getCriteria str
    occurrences = letterOccurrences (getPassword str) letter
    matches = (occurrences >= minAmount) && (occurrences <= maxAmount) 

matchesCriteriaPartTwo :: String -> Bool
matchesCriteriaPartTwo str = matches where
    (minAmount, maxAmount, letter) = getCriteria str
    password = getPassword str
    occurrences = letterOccurrences password letter
    checkDigitOne = password !! (minAmount-1)
    checkDigitTwo = password !! (maxAmount-1)
    matches = xor (letter == checkDigitOne) (letter == checkDigitTwo)

getPassword :: String -> String
getPassword str = drop 2 (dropWhile (/=':') str)

getCriteria :: String -> (Int, Int, Char)
getCriteria str = (minAmount, maxAmount, letter) where
    criteria = takeWhile (/=' ') str
    minAmount = read (takeWhile (/='-') criteria ) :: Int
    maxAmount = read (dropWhile (=='-') (dropWhile (/='-') criteria)) :: Int
    letter = takeWhile (/= ':') (dropWhile (/=' ') str) !! 1

letterOccurrences :: String -> Char -> Int
letterOccurrences str x = length (filter (==x) str)