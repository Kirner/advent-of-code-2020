let fs = require("fs")
let eol = require("os").EOL

const lines = fs.readFileSync("day-10-input.txt").toString().split(eol)
const numbers = lines.map(o => Number(o))

function partOne(numbers) {
    numbers.push(0)
    numbers.sort((a, b) => a - b)
    let ones = 0
    let threes = 1
    for (i = 0; i < numbers.length-1; i++ ) {
        if (numbers[i+1] == numbers[i]+1)
            ones++
        else if (numbers[i+1] == numbers[i]+3)
            threes++
    }
    return ones * threes
}

let knownPermutations = {}
function partTwo(numbers) {
    numbers.push(0)
    numbers.sort((a, b) => a - b)
    return calculatePermutations(numbers)
}

function calculatePermutations(numbers, index = 0) {
    if (knownPermutations[index] != undefined)
        return knownPermutations[index];

    if (index >= numbers.length)
        return 0;

    if ((numbers.length - index) < 2)
        return 1;

    knownPermutations[index+1] = calculatePermutations(numbers, index+1, knownPermutations);

    permutations = knownPermutations[index+1];

    if ((numbers[index+2] - numbers[index]) <= 3) {
        knownPermutations[index+2] = calculatePermutations(numbers, index+2);
        permutations += knownPermutations[index+2];
    }

    if ((numbers[index+3] - numbers[index]) <= 3) {
        knownPermutations[index+3] = calculatePermutations(numbers, index+3);
        permutations += knownPermutations[index+3];
    }

    return permutations;
}

console.log("Part One:", partOne(numbers.slice()))
console.log("Part Two:", partTwo(numbers.slice()))