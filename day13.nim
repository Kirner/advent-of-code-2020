import strutils
import sequtils, sugar
from math import floor

proc dayOneInput(): (int, seq[int]) =
    let f = open("day-13-input.txt")
    defer: f.close()
    let timestamp = parseInt($f.readLine())
    let line = $f.readLine()
    let ids = line.replace(",x", "").split({','}).map(x => parseInt(x))
    return (timestamp, ids)

proc dayTwoInput(): seq[(int, int)] =
    let f = open("day-13-input.txt")
    defer: f.close()
    let _ = $f.readLine()
    let line = $f.readLine()
    var deltasWithIds = newSeq[(int, int)]()
    let split = line.split({','})
    for delta in 0..len(split)-1:
        if split[delta] == "x": continue
        deltasWithIds.add((delta, parseInt(split[delta])))
    deltasWithIds

proc closestMultiple(n: int, timestamp: int): (int, int) = 
    let divByTimestamp = floor(timestamp / n).toInt()
    if ((divByTimestamp*n) >= timestamp):
        (n,timestamp)
    else:
        (n, (divByTimestamp + 1) * n)

proc dayOne(): (int) =
    let (timestamp, ids) = dayOneInput()
    let idsWithValues = ids.map(x => closestMultiple(x, timestamp))
    var (lowestId, lowest) = idsWithValues[0]
    for (id, value) in idsWithValues:
        if (value < lowest):
            lowest = value
            lowestId = id
    (lowest - timestamp) * lowestId

proc dayTwo(): (int) =
    var input = dayTwoInput()
    var step = input[0][1]
    var time_stamp = 0
    input.delete(0)
    for (delta, bus_id) in input:
        while ((time_stamp + delta) mod bus_id) != 0:
            time_stamp += step
        step *= bus_id
    return time_stamp

echo dayTwo()
