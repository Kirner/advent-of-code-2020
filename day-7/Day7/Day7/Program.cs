﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day7
{
    class Program
    {
        Dictionary<string,Bag> bagsByColor = new Dictionary<string, Bag>();

        static void Main(string[] args)
        {
            var filePath = "day-7-input.txt";
            string[] lines = File.ReadAllLines(filePath);
            BagParser bagParser = new BagParser();

            foreach (var line in lines)
            {
                bagParser.Parse(line);
            }

            Console.WriteLine($"Part one: {bagParser.BagsContainingGold}");
            Console.WriteLine($"Part two: {bagParser.bagsByColor["shiny gold"].NumberOfBags() -1 }");
        }
    }

    public class Bag {
        public sealed class Child 
        {
            public Child(Bag bag, int numberOfBags)
            {
                Bag = bag;
                NumberOfBags = numberOfBags;
            }

            public Bag Bag;
            public int NumberOfBags;
        }

        public Bag(string color)
        {
            Color = color;
        }

        public readonly string Color;
        public readonly List<Child> children = new List<Child>();

        public bool HasGold => Color == "shiny gold" || children.Any(o => o.Bag.HasGold);

        internal int NumberOfBags()
        {
            int number = 1;
            foreach (var child in children)
            {
                number += (child.NumberOfBags * child.Bag.NumberOfBags());
            }
            return number;
        }
    }

    internal sealed class BagParser
    {
        public Dictionary<string,Bag> bagsByColor = new Dictionary<string, Bag>();

        public int BagsContainingGold => bagsByColor.Values.Count(o => o.HasGold) - bagsByColor.Values.Count(o => o.Color == "shiny gold");

        public void Parse(string line) { 
            var pattern = @"^(\w+ \w+) bags contain ((?:\d|no other)) (\w+ \w+)?\s?bags?";
            var match = Regex.Match(line, pattern);
            var color = match.Groups[1].Value;

            var bag = GetBag(color);

            var amountContained = match.Groups[2].Value;
            if (amountContained == "no other") return;

            var childBag = GetBag(match.Groups[3].Value);
            bag.children.Add(new Bag.Child(childBag , int.Parse(amountContained)));
            foreach (var bagDescriptor in line.Replace(".", String.Empty).Split(", ").Skip(1)) {
                int number = int.Parse(bagDescriptor[..2]);
                string bagColor = bagDescriptor[2..].Replace(" bags", string.Empty).Replace(" bag", string.Empty);
                bag.children.Add(new Bag.Child(GetBag(bagColor), number));
            }
        }

        private Bag GetBag(string color) {
            bagsByColor.TryGetValue(color, out Bag bag);
            if (bag == null)
            {
                bag = new Bag(color);
                bagsByColor[bag.Color] = bag;
            }
            return bag;
        }
    }
}
