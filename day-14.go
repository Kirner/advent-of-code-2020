package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"
	"strconv"
	"sort"
)

func readLines() []string {
	file, err := os.Open("day-14-input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	lines := []string{}
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return lines
}

func maskPermutations(mask string) []int64 {
	a := []int{}
	for i, char := range mask {
		if char == 'X' {
			a = append(a, 1<<(len(mask)-i-1))
		}
	}

	permutations := []int{0}
	for _, i := range a {
		for _, j := range permutations {
			permutations = append(permutations, i|j)
		}
	}

	sort.IntSlice(permutations).Sort()
	out := []int64{}
	for _, val := range permutations {
		out  = append(out, int64(val))
	}
	return out
}

func partOne(lines []string) int64 {
	mem := map[int64]int64{}
	mask := int64(0)
	maskFlipped := int64(0)

	for _, line := range lines {
		match, _ := regexp.MatchString("mask", line)
		if match {
			maskStr := strings.Replace(line, "mask = ", "", -1)
			mask, _ = strconv.ParseInt(strings.Replace(maskStr, "X", "0", -1), 2, 0)
			maskFlipped, _ = strconv.ParseInt(strings.NewReplacer("1", "0", "X", "1").Replace(maskStr), 2, 0)
		} else {
			re := regexp.MustCompile(`^mem\[(\d+)\] = (\d+)$`)
			found := re.FindStringSubmatch(line)
			pos, _ := strconv.ParseInt(found[1], 10, 0)
			value, _ := strconv.ParseInt(found[2], 10, 0)
			value = mask | (maskFlipped & value)
			mem[pos] = value
		}
	}

	out := int64(0)
	for _, value := range mem {
		out += value
	}
	return out
}

func partTwo(lines []string) int64 {
	mem := map[int64]int64{}
	mask := int64(0)
	maskFlipped := int64(0)
	permutations := []int64{}
	for _, line := range lines {
		match, _ := regexp.MatchString("mask", line)
		if match {
			maskStr := strings.Replace(line, "mask = ", "", -1)
			mask, _ = strconv.ParseInt(strings.Replace(maskStr, "X", "0", -1), 2, 0)
			maskFlipped, _ = strconv.ParseInt(strings.NewReplacer("X", "0", "1", "0", "0", "1").Replace(maskStr), 2, 0)
			permutations = maskPermutations(maskStr)
		} else {
			re := regexp.MustCompile(`^mem\[(\d+)\] = (\d+)$`)
			found := re.FindStringSubmatch(line)
			pos, _ := strconv.ParseInt(found[1], 10, 0)
			value, _ := strconv.ParseInt(found[2], 10, 0)
			for _, floatMask := range permutations {
				pos = mask | (maskFlipped & pos) | floatMask
				mem[pos] = value
			}
		}
	}

	out := int64(0)
	for _, value := range mem {
		out += value
	}
	return out
}

func main() {
	fmt.Println(partOne(readLines()))
	fmt.Println(partTwo(readLines()))
}