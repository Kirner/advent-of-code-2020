module Main exposing (..)

import Html exposing (text, p, div, h1)
import Debug exposing (log)
import Dict
import Tuple
import List
import String

main =
    let  
        inputMap = mappedInput
        one = p [] [text "Day one : ", text (String.fromInt (partOne inputMap))]
        two = p [] [text "Day two : ", text (String.fromInt (partTwo inputMap))]
    in div [] [(h1 [] [text "Advent of code day 15!"]), one, two]

partOne inputMap = 
    let
        largestInput = Maybe.withDefault 0 (List.maximum (Dict.keys inputMap)) 
        nextIndex = List.length (Dict.keys inputMap) + 1
    in
        nextNthNumber inputMap largestInput nextIndex 2020

partTwo inputMap = 
    let
        largestInput = Maybe.withDefault 0 (List.maximum (Dict.keys inputMap)) 
        nextIndex = List.length (Dict.keys inputMap) + 1
    in
        nextNthNumber inputMap largestInput nextIndex 30000000

nextNthNumber inputMap num index stopAt =
    let 
        nextResult = nextInputState inputMap num index
        nextMap = Tuple.first nextResult 
        nextNum = Tuple.second nextResult 
        nextIndex = index + 1
    in if nextIndex == (stopAt + 1) then
        nextNum 
    else
        nextNthNumber nextMap nextNum nextIndex stopAt

nextInputState inputMap num index = 
    let
        lastPos = lastPositionOfNumber inputMap num
        nextNum = nextNumber inputMap num
        nextNumLastPos = lastPositionOfNumber inputMap nextNum 
        newMap = if nextNumLastPos == 0 then
                Dict.update nextNum (\_ -> Just (index, index)) inputMap 
            else
                Dict.update nextNum (\_ -> Just (index, nextNumLastPos)) inputMap 
    in (newMap, nextNum)

nextNumber inputMap num =
    let positions = Dict.get num inputMap 
            |> Maybe.withDefault (0, 0)
    in (Tuple.first positions) - (Tuple.second positions)

lastPositionOfNumber inputMap num =
    Dict.get num inputMap 
    |> Maybe.withDefault (0, 0)
    |> Tuple.first

mappedInput = 
    let day15Input = [0,5,4,1,10,14,7]
        naturalNumbers = List.range 1 (List.length day15Input)
    in 
        List.map2 Tuple.pair naturalNumbers naturalNumbers 
        |> List.map2 Tuple.pair day15Input
        |> Dict.fromList