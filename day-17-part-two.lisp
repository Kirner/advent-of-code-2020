(defun axis-variation (point)
  (list (- point 1) point (+ point 1)))

(defun group-veriations (veriations &optional n)
  (let ((head (car veriations))
        (tail (cdr veriations))
        (prepend-n #'(lambda (li) (cons n li))))
    (if n
        (if tail
            (loop for x in head
                  append (mapcar prepend-n (group-veriations tail x)))
            (mapcar #'(lambda (x) (list n x)) head))
        (loop for x in head
              append (group-veriations tail x)))))

(defun neighbour-coordinates (cords)
  (let ((axis-variations (mapcar #'axis-variation cords)))
    (loop for coordinate in (group-veriations axis-variations)
          if (not (equal cords coordinate))
          collect coordinate)))

(defun nth-or-nil (n li)
  (cond ((< n 0) nil)
        ((>= n (length li)) nil)
        ((equal li nil) nil)
        ((typep li 'list) (nth n li))
        ((typep li 'string) (char li n))))

(defun active-cell (cords grid)
   (let ((head (car cords))
         (tail (cdr cords)))
     (cond ((not grid) nil)
           (tail       (active-cell tail (nth-or-nil head grid)))
           (t          (equal #\# (nth-or-nil head grid))))))

(defun active-neighbours (coords grid)
  (length (let ((neighbour-coords (neighbour-coordinates coords)))
    (remove-if-not #'(lambda (x) (active-cell x grid)) neighbour-coords))))

(defun next-cell-state (point grid)
  (let* ((num-neighbours (active-neighbours point grid))
         (active (active-cell point grid)))
      (cond ((and active
                  (or (= num-neighbours 2)
                      (= num-neighbours 3))) #\#)
            ((and (not active)
                  (= num-neighbours 3)) #\#)
            (t #\.))))

(defun get-grid-coordinate (grid coords)
  (cond ((not coords) grid)
        ((cdr coords) (get-grid-coordinate (nth-or-nil (car coords) grid) (cdr coords)))
        (t (nth-or-nil (car coords) grid))))

(defun next-state (grid &optional coords)
  (let ((current-grid (get-grid-coordinate grid coords)))
    (cond ((typep current-grid 'string)
           (coerce (loop for i from 0 to (- (length current-grid) 1)
                        collect(next-cell-state (append coords (list i)) grid)) 'string))
          ((typep current-grid 'list)
           (loop for i from 0 to (- (length current-grid) 1)
                 collect (next-state grid (append coords (list i))))))))

(defun make-line (len)
  (make-string len :initial-element '#\.))

(defun blank-copy (grid)
  (cond ((typep grid 'string) (make-line (length grid)))
        ((typep grid 'list) (mapcar #'blank-copy grid))))

(defun expand-grid (grid)
  (cond ((typep grid 'string) (concatenate 'string "." grid "."))
        ((typep grid 'list)
           (let* ((new-grid (mapcar #'expand-grid grid))
                  (blank (blank-copy (car new-grid))))
             (append (list blank) new-grid (list blank))))))

(defun active-in-grid (grid)
  (cond ((typep grid 'string) (count #\# grid))
        ((typep grid 'list) (reduce #'+ (mapcar #'active-in-grid grid)))))

(defun read-lines (filepath)
  (with-open-file (in filepath)
    (loop for line = (read-line in nil nil)
      while line
      collect line)))

(defun anwser (grid n)
  (if (= n 6)
      (active-in-grid grid)
      (anwser (next-state (expand-grid grid)) (+ n 1))))

(defparameter 3d-grid (list (read-lines "day-17-input.txt")))
(defparameter 4d-grid (list (list (read-lines "day-17-input.txt"))))

(print (anwser 3d-grid 0))
(print (anwser 4d-grid 0))
